let prisonersList = [],
    attendees = 0,
    daysCounter = 0,
    bulbIsOn = false,
    warden = {
        choosePrisoner: () => {
            daysCounter++;
            let prisonerID = Math.floor(Math.random() * 100);
            prisonersList[prisonerID].visitChamber();
        }
    },
    lead = {
        attendedChamber: false,
        visitChamber: function () {
            if (!this.attendedChamber) {
                this.attendedChamber = true;
                attendees++;
            }
            if (bulbIsOn) {
                bulbIsOn = false;
                attendees++;
            }
        }
    };

function Prisoner() {
    this.attendedChamber = false;
    this.visitChamber = function () {
        if (!bulbIsOn && !this.attendedChamber) {
            bulbIsOn = true;
            this.attendedChamber = true;
        }
    }
}

(function populatePrison() {
    prisonersList.push(lead);
    while (prisonersList.length < 100) {
        prisonersList.push(new Prisoner());
    }
    while (attendees !== 100) {
        warden.choosePrisoner();
    }
    console.log(`It took ${daysCounter} days to escape.`);
})();