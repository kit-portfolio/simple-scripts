let scorePoints = 0;

const button = document.querySelector('.clickerButton');
const pewContainer = document.querySelector('.pewContainer');
const highScore = parseInt(localStorage.highScore) || 0;
const greetingText = (highScore > 0) ? `Current high score is ${highScore}. Wanna test your might?`
        : "Looks like no one before had enough courage to be a champion. This is your chance to be a hero!";

(() => pewContainer.insertAdjacentHTML('beforebegin', `<p>${greetingText}<\p>`))();

button.addEventListener('click', score);

function score() {
    if (scorePoints === 0) setTimeout(finish, 5000);
    scorePoints++;
    pewContainer.insertAdjacentHTML('afterbegin', 'PEW! ');
}

function finish() {
    button.removeEventListener('click', score);
    let header, resultDescription,
        scoreBoard = `<p>High score: ${highScore}. Your score is ${scorePoints}.</p>`;

    if (highScore < scorePoints) {
        localStorage.setItem('highScore', scorePoints);
        header = `<h1 class="victory">CONGRATULATIONS!</h1>`;
        resultDescription = `<p>You did it! You are the number one!</p>`;
    } else if (highScore === scorePoints) {
        header = `<h1 class="draw">IT'S A DRAW!</h1>`;
        resultDescription = `<p>Good try!</p>`;
    } else if (highScore > scorePoints) {
        header = `<h1 class="defeat">YOU FAILED!</h1>`;
        resultDescription = `<p>Sentenced to death... By SNU-SNU!</p>`;
    }

    document.write(`<title>Results</title><link rel="stylesheet" href="styles.css"><div class="mainContainer">
        ${header}
        ${scoreBoard}
        ${resultDescription}
        <input class="resetButton" type="button" value="RESET" onclick="document.location.reload(true);">
        </div>
    `);
}
