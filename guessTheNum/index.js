const randomNumber = Math.floor(Math.random() * 100);
let counter = 1;

console.log('Guess the number!');

process.stdin.on('data', (userInput) => {
    if (userInput == randomNumber) {
        console.log(`You guessed after ${counter} attempts.`);
        process.exit();
    }
    if (userInput < randomNumber) console.log('Too small');
    if (userInput > randomNumber) console.log('Too big');
    counter++;
});