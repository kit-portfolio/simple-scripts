const WebSocket = require('ws');

const server = new WebSocket.Server({port: 3000});

server.on('connection', ws => {
    ws.on(`message`, message => {
        let data = JSON.parse(message);

        if (data.text === 'exit') {
            ws.close();
            sendMessage(`${data.author} has left the chat.`);
            return;
        }

        sendMessage(`${data.author}: ${data.text}`);
    });

    ws.send(`Welcome!`);
});

function sendMessage(message) {
    server.clients.forEach(client => {
        if (client.readyState === WebSocket.OPEN) {
            client.send(message);
        }
    })
}

console.log(`Server is up and running`);