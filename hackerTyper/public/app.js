const status = document.getElementById('status'),
    messages = document.getElementById('messages'),
    form = document.getElementById('form'),
    input = document.getElementById('input');

const ws = new WebSocket(`ws://localhost:3000`),
    username = prompt('Enter your name') || `Anonymous`;

function setStatus(value) {
    status.innerHTML = value;
}

function printMessage(value) {
    const li = document.createElement('li');

    li.innerHTML = value;
    messages.appendChild(li);
}

form.addEventListener(`submit`, event => {
    event.preventDefault();
    ws.send(`{"author": "${username}", "text": "${input.value}"}`);
    input.value = ``;
});

ws.onopen = () => {
    setStatus(`ONLINE`);
    ws.send(`{"author": "System", "text": "${username} has joined us."}`);
};
ws.onclose = () => setStatus(`DISCONNECTED`);
ws.onmessage = response => printMessage(response.data);