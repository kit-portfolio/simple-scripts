In this repository I'm conducting vicious experiments on various interview puzzles and other things I want to implement in code.

# click4pew
This one is really tough! Pass extreme 5-second challenge to define who is the fastest clicker in da house! Click 4 PEW, bro!

# Guess the num
How many attempts does it take you to guess random number from 0 to 100?

# Hacker Typer
Websocket-based chat. Long story short: one day I decided to get my hands on websocket technology, and this marvellous piece of software appeared. It can share messages with all clients and know how to do a system notification, when somebody join or leave the chat.

# Prison break
Solution for famous Microsoft puzzle.
> 100 prisoners are stuck in the prison in solitary cells. The warden of the prison got bored one day and offered them a challenge. He will put one prisoner per day, selected at random (a prisoner can be selected more than once), into a special room with a light bulb and a switch which controls the bulb. No other prisoners can see or control the light bulb. The prisoner in the special room can either turn on the bulb, turn off the bulb or do nothing. On any day, the prisoners can stop this process and say “Every prisoner has been in the special room at least once”. If that happens to be true, all the prisoners will be set free. If it is false, then all the prisoners will be executed. The prisoners are given some time to discuss and figure out a solution. How do they ensure they all go free?

This script prints out how many days it took prisoners to solve warden's puzzle and get their freedom.   